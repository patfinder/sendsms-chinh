﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Nexle.Common
{
    public class Crypto
    {
        //private static byte[] ent = Encoding.UTF8.GetBytes("CmcOnTheWorld");
        /// <summary>
        /// This should be initialized once and only one.
        /// </summary>
        private static string _Entropy = "NexleLib Entropy";
        public static string Entropy
        {
            get
            {
                if(string.IsNullOrEmpty(_Entropy))
                    throw new InvalidOperationException("_Entropy not set");

                return _Entropy;
            }
        }

        /// <summary>
        /// Securely encrypt and return base-64 string
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        public static string EncryptB64(string plainText)
        {
            return EncryptB64(Entropy, plainText);
        }

        /// <summary>
        /// Encrypt and return base-64 string
        /// </summary>
        /// <param name="entropy"></param>
        /// <param name="plainText"></param>
        /// <returns></returns>
        public static string EncryptB64(string entropy, string plainText)
        {
            if (string.IsNullOrEmpty(entropy))
                throw new ArgumentNullException("entropy");

            if (plainText == null)
                throw new ArgumentNullException("plainText");

            byte[] ent = Encoding.UTF8.GetBytes(entropy);

            // Encrypt data
            byte[] data = Encoding.Unicode.GetBytes(plainText);
            byte[] encrypted = ProtectedData.Protect(data, ent, DataProtectionScope.LocalMachine);

            // Return as base64 string
            return Convert.ToBase64String(encrypted);
        }

        /// <summary>
        /// Decrypt secured base-64 encrypted string
        /// </summary>
        /// <param name="cipher"></param>
        /// <returns></returns>
        public static string DecryptB64(string cipher)
        {
            return DecryptB64(Entropy, cipher);
        }

        /// <summary>
        /// Decrypt secured base-64 encrypted string
        /// </summary>
        /// <param name="entropy"></param>
        /// <param name="cipher"></param>
        /// <returns></returns>
        public static string DecryptB64(string entropy, string cipher)
        {
            if (string.IsNullOrEmpty(entropy))
                throw new ArgumentNullException("entropy");

            if (string.IsNullOrEmpty(cipher))
                throw new ArgumentNullException("cipher");

            // Parse base64 string
            byte[] data = Convert.FromBase64String(cipher);

            byte[] ent = Encoding.UTF8.GetBytes(entropy);

            // Decrypt data
            byte[] decrypted = ProtectedData.Unprotect(data, ent, DataProtectionScope.LocalMachine);
            return Encoding.Unicode.GetString(decrypted);
        }

        /// <summary>
        /// Securely encrypt and return Base-64 string
        /// </summary>
        /// <param name="plainText"></param>
        /// <param name="Key"></param>
        /// <param name="IV"></param>
        /// <returns></returns>
        public static string EncryptAESB64(string plainText, byte[] Key, byte[] IV)
        {
            return Convert.ToBase64String(EncryptAES(plainText, Key, IV));
        }

        /// <summary>
        /// Securely encrypt a string
        /// </summary>
        /// <param name="plainText"></param>
        /// <param name="Key"></param>
        /// <param name="IV"></param>
        /// <returns></returns>
        public static byte[] EncryptAES(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");
            byte[] encrypted;

            // Create an AesManaged object
            // with the specified key and IV. 
            using (AesManaged aes = new AesManaged())
            {
                aes.Key = Key;
                aes.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            // Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }

                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            // Return the encrypted bytes from the memory stream. 
            return encrypted;
        }

        /// <summary>
        /// Decrypt a base-64 AES data
        /// </summary>
        /// <param name="base64Text"></param>
        /// <param name="Key"></param>
        /// <param name="IV"></param>
        /// <returns></returns>
        public static string DecryptAESB64(string base64Text, byte[] Key, byte[] IV)
        {
            return DecryptAES(Convert.FromBase64String(base64Text), Key, IV);
        }

        /// <summary>
        /// AES decrypt data
        /// </summary>
        /// <param name="cipherText"></param>
        /// <param name="Key"></param>
        /// <param name="IV"></param>
        /// <returns></returns>
        public static string DecryptAES(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");

            // Declare the string used to hold 
            // the decrypted text. 
            string plaintext = null;

            // Create an AesManaged object 
            // with the specified key and IV. 
            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption. 
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            // Read the decrypted bytes from the decrypting stream 
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return plaintext;
        }
    }
}
