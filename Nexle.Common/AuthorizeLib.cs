﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nexle.Common
{
    public class AuthorizeLib
    {
        static Random rand = new Random((int)DateTime.Now.Ticks);

        public static string GetRandomToken()
        {
            string guid = Guid.NewGuid().ToString();
            guid = guid.Substring(1, guid.Length - 2);

            List<string> tokens = guid.Split('-').ToList();
            List<string> tokens2 = tokens.Take(2).ToList();
            tokens = tokens.Skip(2).ToList();

            tokens.AddRange(new string[] { string.Format("{0:X4}", rand.Next(1000)).ToLower() });
            tokens.AddRange(tokens2);

            return string.Join("-", tokens.ToArray());
        }
    }
}
