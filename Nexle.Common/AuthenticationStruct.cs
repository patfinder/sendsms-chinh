﻿using System;
using System.Text;
using System.Security.Cryptography;
using Newtonsoft.Json;

namespace Nexle.Common
{
    public class AuthenticationStruct
    {
        private const int MIN_KEY_LENGTH = 40;
        public DateTime TimeStamp = DateTime.MinValue;
        public int UserID = 0;
        public string Name = null;
        public string Authenticator = null;

        private static string GetAuthenticator(AuthenticationStruct authStruct)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            string keyString = authStruct.Name;

            // Add string to long string 
            var strAdd = string.Empty;
            for(var i=0; i< 40 ; i++) strAdd += "a";
            keyString = keyString.Length < 40 ? keyString + strAdd : keyString;

            byte[] md5Hash = md5.ComputeHash(new UTF8Encoding().GetBytes(keyString));

            return Convert.ToBase64String(md5Hash);
        }

        public static AuthenticationStruct FromUser(int ID, string name)
        {
            AuthenticationStruct authStruct = new AuthenticationStruct()
            {
                TimeStamp = DateTime.Now,
                UserID = ID,
                Name = name
            };

            authStruct.Authenticator = GetAuthenticator(authStruct);
            return authStruct;
        }

        public string GetAccessToken(byte[] key, byte[] IV)
        {
            return Crypto.EncryptAESB64(ToJsonString(), key, IV);
        }

        public bool Authenticate()
        {
            return Authenticator == GetAuthenticator(this);
        }

        public string ToJsonString()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;

            return JsonConvert.SerializeObject(this, settings);
        }

        public static AuthenticationStruct FromJsonString(string json)
        {
            try
            {
                return (AuthenticationStruct)JsonConvert.DeserializeObject(json, typeof(AuthenticationStruct));
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
