﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
//using System.Threading.Tasks;
using System.Web;

namespace Nexle.Common
{
    class NetUtils
    {
        public static WebRequest CreateRequest(string requestUrl, string accessToken)
        {
            WebRequest webRequest = WebRequest.Create(requestUrl);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/json";

            //string data = HttpUtility.UrlEncode(JsonConvert.SerializeObject(new { ids = pageIDs, week = weekStr }));
            string data = HttpUtility.UrlEncode(JsonConvert.SerializeObject(new { }));

            byte[] bytes = Encoding.ASCII.GetBytes(data);
            Stream dataStream = webRequest.GetRequestStream();
            dataStream.Write(bytes, 0, bytes.Length);
            dataStream.Close();

            webRequest.Headers.Add("accesstoken", accessToken);
            return webRequest;
        }
    }
}
