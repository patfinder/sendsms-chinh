﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace Nexle.Common
{
    public class TimeUtils
    {
        public static string GetDiffString(DateTime dateTime)
        {
            return GetDiffString(DateTime.Now - dateTime);
        }

        public static string GetDiffString(TimeSpan timeDiff)
        {
            int seconds = (int)timeDiff.TotalSeconds;
            const int SECOND = 1;
            const int MINUTE = 60 * SECOND;
            const int HOUR = 60 * MINUTE;
            const int DAY = 24 * HOUR;
            const int MONTH = 30 * DAY;

            if (seconds < 0)
            {
                return "not yet";
            }
            if (seconds < 1 * MINUTE)
            {
                return timeDiff.Seconds == 1 ? "one second ago" : timeDiff.Seconds + " seconds ago";
            }
            if (seconds < 2 * MINUTE)
            {
                return "a minute ago";
            }
            if (seconds < 45 * MINUTE)
            {
                return timeDiff.Minutes + " minutes ago";
            }
            if (seconds < 90 * MINUTE)
            {
                return "an hour ago";
            }
            if (seconds < 24 * HOUR)
            {
                return timeDiff.Hours + " hours ago";
            }
            if (seconds < 48 * HOUR)
            {
                return "yesterday";
            }
            if (seconds < 30 * DAY)
            {
                return timeDiff.Days + " days ago";
            }
            if (seconds < 12 * MONTH)
            {
                int months = Convert.ToInt32(Math.Floor((double)timeDiff.Days / 30));
                return months <= 1 ? "one month ago" : months + " months ago";
            }
            else
            {
                int years = Convert.ToInt32(Math.Floor((double)timeDiff.Days / 365));
                return years <= 1 ? "one year ago" : years + " years ago";
            }
        }

        /// <summary>
        /// Get first day of week of specified day. Default Monday is first day.
        /// </summary>
        /// <param name="weekDate"></param>
        /// <param name="firstDay"></param>
        /// <returns></returns>
        public static DateTime FirstDayOfWeek(DateTime weekDate, DayOfWeek firstDay = DayOfWeek.Monday)
        {
            int delta = firstDay - weekDate.DayOfWeek;
            return weekDate.AddDays(delta);
        }
    }
}
