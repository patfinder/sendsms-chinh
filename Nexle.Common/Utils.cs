﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Newtonsoft.Json;

namespace Nexle.Common
{
    public class Utils
    {
        private static Random random = new Random((int)DateTime.Now.Ticks);
        public static string RandomString(int size, bool lowerCase = false)
        {
            StringBuilder sb = new StringBuilder();
            char c;
            Random rand = new Random();
            for (int i = 0; i < size; i++)
            {
                c = Convert.ToChar(Convert.ToInt32(rand.Next(65, 87)));
                sb.Append(c);
            }
            if (lowerCase)
            {
                return sb.ToString().ToLower();
            }
            return sb.ToString();
        }

        public static void SendEmail(string server, int port, string emailFrom, string user, string password, string[] emailTos, string subject, string body)
        {
            SendEmail(server, port, emailFrom, null, user, password, emailTos, subject, body);
        }

        public static void SendEmailAsync(string server, int port, string emailFrom, string user, string password, string[] emailTos, string subject, string body)
        {
            Thread t = new Thread(delegate()
            {
                SendEmail(server, port, emailFrom, null, user, password, emailTos, subject, body);
            });
            t.Start();
        }

        public static void SendEmail(string server, int port, string emailFrom, string emailTitle, string user, string password, string[] emailTos, string subject, string body)
        {
            emailTitle = emailTitle ?? emailFrom;

            var mail = new System.Net.Mail.MailMessage();
            mail.Subject = subject;
            mail.Sender = new System.Net.Mail.MailAddress(emailFrom, emailTitle);
            mail.From = mail.Sender;
            mail.IsBodyHtml = true;
            mail.Body = body;

            emailTos.ToList().ForEach(a => mail.To.Add(a));

            var smtp = new System.Net.Mail.SmtpClient(server, port)
            {
                EnableSsl = true,
            };
            smtp.Credentials = new System.Net.NetworkCredential(user, password);

            //var smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587)
            //{
            //    EnableSsl = true,
            //};
            //smtp.Credentials = new System.Net.NetworkCredential("vuong.se@gmail.com", "2NYG00gle");

            smtp.Send(mail);
        }

        public static T GetObjectFromJson<T>(string json)
        {
            return ((T)JsonConvert.DeserializeObject(json, typeof(T)));
        }

        public static object GetObjectFromJson(string json, Type type)
        {
            return JsonConvert.DeserializeObject(json, type);
        }

        public static string GetJsonFromObject(object obj)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;

            return JsonConvert.SerializeObject(obj, settings);
        }
    }
}
