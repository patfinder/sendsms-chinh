﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace Nexle.Common
{
    public static class StringUtils
    {
        public static string Ellipsis(this string source, int length)
        {
            if (source == null)
                return source;

            if (source.Length <= length)
                return source;

            return source.Substring(0, length - 4) + " ...";
        }
    }
}
