﻿using SendSMS.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SendSMS
{
    public partial class FrmDetectModems : Form
    {
        private GroupBox[] ModemGroups = new GroupBox[3];
        private ComboBox[] NetworkCombos = new ComboBox[3];
        private Label[] COMLabels = new Label[3];
        private CheckBox[] DefaultChecks = new CheckBox[3];

        public FrmDetectModems()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Init groups to invisible
        /// </summary>
        private void InitGroups()
        {
            ModemGroups.ToList().ForEach(g =>
            {
                g.Visible = false;
            });

            // Reset network selection.
            for (int i = 0; i < 3; i++)
            {
                NetworkCombos[i].SelectedIndex = -1;
                COMLabels[i].Text = "";
                DefaultChecks[i].Checked = false;
            }
        }

        private void LoadCurrentConfig()
        {
            InitGroups();
            int activeGroup = 0;
            for(int i=0; i<3; i++)
            {
                if( !string.IsNullOrEmpty((string)Settings.Default["Modem" + (i + 1)]) && 
                    !string.IsNullOrEmpty((string)Settings.Default["Network" + (i + 1)]))
                {
                    ModemGroups[activeGroup].Visible = true;
                    COMLabels[activeGroup].Text = (string)Settings.Default["Modem" + (i + 1)];
                    NetworkCombos[activeGroup].Text = (string)Settings.Default["Network" + (i + 1)];
                    
                    activeGroup++;
                }
            }

            if( Settings.Default.DefaultModem >= 1 &&
                Settings.Default.DefaultModem <= 3)
            {
                DefaultChecks[Settings.Default.DefaultModem - 1].Checked = true;
            }
            else
            {
                DefaultChecks[0].Checked = true;
            }
        }

        private void FrmDetectModems_Load(object sender, EventArgs e)
        {
            // Hide modems initially
            ModemGroups[0] = gbxModem1;
            ModemGroups[1] = gbxModem2;
            ModemGroups[2] = gbxModem3;

            COMLabels[0] = lblComName1;
            COMLabels[1] = lblComName2;
            COMLabels[2] = lblComName3;

            NetworkCombos[0] = cboNetwork1;
            NetworkCombos[1] = cboNetwork2;
            NetworkCombos[2] = cboNetwork3;

            DefaultChecks[0] = chkDefault1;
            DefaultChecks[1] = chkDefault2;
            DefaultChecks[2] = chkDefault3;

            // Get networks list
            List<string> networks = new List<string>();
            foreach(string net in Settings.Default.KnownNetworks)
                networks.Add(net);

            // Init network combos
            for (int i = 0; i < 3; i++)
            {
                NetworkCombos[i].Items.Clear();
                networks.ForEach(net => NetworkCombos[i].Items.Add(net));
            }

            // Show current values
            for (int i = 0; i < 3; i++)
            {
                COMLabels[i].Text = (string)Settings.Default["Modem" + (i + 1)];

                string net = (string)Settings.Default["Network" + (i + 1)];
                if (networks.Contains(net))
                    NetworkCombos[i].SelectedIndex = networks.IndexOf(net);
                else
                    NetworkCombos[i].SelectedIndex = -1;
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            // Hide groups
            InitGroups();
            
            List<string> modemPorts = new List<string>();

            // Start detecting modems
            foreach (string portName in SerialPort.GetPortNames())
            {
                if (SendSMSLib.IsModem(portName))
                {
                    modemPorts.Add(portName);
                }
            }

            if (modemPorts.Count <= 0)
            {
                MessageBox.Show("No Modem found!");
                return;
            }

            // Set Modem content and show modem info (control groups)
            for(int i = 0; i < modemPorts.Count; i++) {
                ModemGroups[i].Visible = true;
                COMLabels[i].Text = modemPorts[i];
                NetworkCombos[i].SelectedIndex = -1;
            }
        }

        private void chkDefault_CheckedChanged(object sender, EventArgs e)
        {
            // Clear other default checks
            for (int i = 0; i < 3; i++)
            {
                if (DefaultChecks[i] != sender)
                    DefaultChecks[i].Checked = false;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            // Check if network names provided
            for(int i=0; i<3; i++)
            {
                if(ModemGroups[i].Visible && NetworkCombos[i].SelectedIndex < 0)
                {
                    MessageBox.Show("Please select network name for all the model(s).");
                    return;
                }
            }

            // Save modem config
            for (int i = 0; i < 3; i++)
            {
                bool visible = ModemGroups[i].Visible;
                Settings.Default["Modem" + (i + 1)] = visible ? COMLabels[i].Text : "";
                Settings.Default["Network" + (i + 1)] = visible ? (string)NetworkCombos[i].SelectedItem : "";
            }

            Settings.Default.Save();
        }

    }
}
