﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Finisar.SQLite;

namespace SendSMS
{
    public class NumberPhoneModel
    {
        public string NumberPhone { get; set; }
        public string Content { get; set; }
    }

    public class SendMessageHistory : NumberPhoneModel
    {
//        public int Id { get; set; }
/*        public string NumberPhone { get; set; }
        public string Content { get; set; }*/
        public string MessageTime { get; set; }
    }

    public class DbService
    {
        private SQLiteCommand sql_cmd;
        private SQLiteConnection sql_con;

/*        public void LoadData()
        {
            SetConnection();

//            ExecuteQuery("CREATE TABLE History (Id INTEGER PRIMARY KEY,NumberPhone TEXT DEFAULT NULL,Content TEXT DEFAULT NULL, SendTime Text Default NULL)");
//            ExecuteQuery("CREATE TABLE Logs (Id INTEGER PRIMARY KEY,NumberPhone TEXT DEFAULT NULL,Content TEXT DEFAULT NULL, SendTime datetime Default NULL, ErrorMessage TEXT DEFAULT NULL)");

            sql_con.Open();

/*            sql_cmd = sql_con.CreateCommand();
            string CommandText = "select id, desc from  mains";
            DB = new SQLiteDataAdapter(CommandText, sql_con);
            DS.Reset();
            DB.Fill(DS);
            DT = DS.Tables[0];
//            Grid.DataSource = DT;
            sql_con.Close();#1#
        }*/

        private void SetConnection()
        {
//            sql_con = new SQLiteConnection("Data Source=SendSMS.db;Version=3;New=True;Compress=True;");
            if (sql_con == null)
                sql_con = new SQLiteConnection("Data Source=SendSMS.db;Version=3;New=False;Compress=True;");
        }

        private void ExecuteQuery(string txtQuery)
        {
            SetConnection();
            sql_con.Open();

            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = txtQuery;

            sql_cmd.ExecuteNonQuery();
            sql_con.Close();
        }

        public bool SaveOutMessage(string numberPhone, string content)
        {
            return SaveMessage(true, numberPhone, content);
        }
        public bool SaveInMessage(string numberPhone, string content)
        {
            return SaveMessage(false, numberPhone, content);
        }

        /// <summary>
        /// Insert in/out message into DB
        /// </summary>
        /// <param name="outMessage">True if this is a out (sent) message. False if in (receive) message.</param>
        /// <param name="numberPhone"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public bool SaveMessage(bool outMessage, string numberPhone, string content)
        {
            try
            {
                string sql = string.Format("INSERT INTO HISTORY (OutMessage, NumberPhone, Content, MessageTime) VALUES ({0}, '{1}', '{2}', '{3}')", 
                    numberPhone, content, DateTime.Now, outMessage ? 1 : 0);

                ExecuteQuery(sql);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public List<SendMessageHistory> GetHistory(string numberPhone, string content, out string errorMessage)
        {
            errorMessage = string.Empty;
            try
            {
                string sql = @"Select Id, NumberPhone, Content, MessageTime from History";
                if (!string.IsNullOrEmpty(numberPhone) || !string.IsNullOrEmpty(content))
                {
                    sql += " where";
                    if (!string.IsNullOrEmpty(numberPhone))
                    {
                        sql += string.Format(" NumberPhone like '%{0}%'", numberPhone);
                        if (!string.IsNullOrEmpty(content))
                        {
                            sql += " and";
                        }
                    }

                    if (!string.IsNullOrEmpty(content))
                    {
                        sql += string.Format(" Content like '%{0}%'", content);
                    }
                }

                sql += " order by MessageTime desc";

                SetConnection();
                sql_con.Open();

                sql_cmd = sql_con.CreateCommand();

                DataSet dataSet = new DataSet();                
                SQLiteDataAdapter dataAdapter = new SQLiteDataAdapter(sql, sql_con);
                dataAdapter.Fill(dataSet);

                DataTable dataTable = dataSet.Tables[0];
                List<SendMessageHistory> sentMessages = dataTable.AsEnumerable().Select(r => new SendMessageHistory
                {
                    NumberPhone = (string) r["NumberPhone"],
                    Content = (string) r["Content"],
                    MessageTime = (string)r["MessageTime"]
                }).ToList();
                sql_con.Close();
                return sentMessages;
            }
            catch (Exception exception)
            {
                errorMessage = exception.Message + exception.StackTrace;
            }
            return new List<SendMessageHistory>();
        }
    }
}