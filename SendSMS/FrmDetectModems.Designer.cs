﻿namespace SendSMS
{
    partial class FrmDetectModems
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStart = new System.Windows.Forms.Button();
            this.gbxModem1 = new System.Windows.Forms.GroupBox();
            this.chkDefault1 = new System.Windows.Forms.CheckBox();
            this.cboNetwork1 = new System.Windows.Forms.ComboBox();
            this.lblComName1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gbxModem2 = new System.Windows.Forms.GroupBox();
            this.chkDefault2 = new System.Windows.Forms.CheckBox();
            this.cboNetwork2 = new System.Windows.Forms.ComboBox();
            this.lblComName2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.gbxModem3 = new System.Windows.Forms.GroupBox();
            this.chkDefault3 = new System.Windows.Forms.CheckBox();
            this.cboNetwork3 = new System.Windows.Forms.ComboBox();
            this.lblComName3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbxModem1.SuspendLayout();
            this.gbxModem2.SuspendLayout();
            this.gbxModem3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(13, 13);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Tìm 3G";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // gbxModem1
            // 
            this.gbxModem1.Controls.Add(this.chkDefault1);
            this.gbxModem1.Controls.Add(this.cboNetwork1);
            this.gbxModem1.Controls.Add(this.lblComName1);
            this.gbxModem1.Controls.Add(this.label1);
            this.gbxModem1.Location = new System.Drawing.Point(13, 58);
            this.gbxModem1.Name = "gbxModem1";
            this.gbxModem1.Size = new System.Drawing.Size(247, 87);
            this.gbxModem1.TabIndex = 1;
            this.gbxModem1.TabStop = false;
            this.gbxModem1.Text = "Modem 1";
            // 
            // chkDefault1
            // 
            this.chkDefault1.AutoSize = true;
            this.chkDefault1.Location = new System.Drawing.Point(146, 46);
            this.chkDefault1.Name = "chkDefault1";
            this.chkDefault1.Size = new System.Drawing.Size(71, 17);
            this.chkDefault1.TabIndex = 4;
            this.chkDefault1.Text = "Mặc định";
            this.chkDefault1.UseVisualStyleBackColor = true;
            this.chkDefault1.CheckedChanged += new System.EventHandler(this.chkDefault_CheckedChanged);
            // 
            // cboNetwork1
            // 
            this.cboNetwork1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNetwork1.FormattingEnabled = true;
            this.cboNetwork1.Location = new System.Drawing.Point(85, 12);
            this.cboNetwork1.Name = "cboNetwork1";
            this.cboNetwork1.Size = new System.Drawing.Size(132, 21);
            this.cboNetwork1.TabIndex = 3;
            // 
            // lblComName1
            // 
            this.lblComName1.AutoSize = true;
            this.lblComName1.Location = new System.Drawing.Point(10, 50);
            this.lblComName1.Name = "lblComName1";
            this.lblComName1.Size = new System.Drawing.Size(45, 13);
            this.lblComName1.TabIndex = 2;
            this.lblComName1.Text = "Com XX";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mạng";
            // 
            // gbxModem2
            // 
            this.gbxModem2.Controls.Add(this.chkDefault2);
            this.gbxModem2.Controls.Add(this.cboNetwork2);
            this.gbxModem2.Controls.Add(this.lblComName2);
            this.gbxModem2.Controls.Add(this.label3);
            this.gbxModem2.Location = new System.Drawing.Point(13, 162);
            this.gbxModem2.Name = "gbxModem2";
            this.gbxModem2.Size = new System.Drawing.Size(247, 87);
            this.gbxModem2.TabIndex = 3;
            this.gbxModem2.TabStop = false;
            this.gbxModem2.Text = "Modem 2";
            // 
            // chkDefault2
            // 
            this.chkDefault2.AutoSize = true;
            this.chkDefault2.Location = new System.Drawing.Point(146, 46);
            this.chkDefault2.Name = "chkDefault2";
            this.chkDefault2.Size = new System.Drawing.Size(71, 17);
            this.chkDefault2.TabIndex = 5;
            this.chkDefault2.Text = "Mặc định";
            this.chkDefault2.UseVisualStyleBackColor = true;
            this.chkDefault2.CheckedChanged += new System.EventHandler(this.chkDefault_CheckedChanged);
            // 
            // cboNetwork2
            // 
            this.cboNetwork2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNetwork2.FormattingEnabled = true;
            this.cboNetwork2.Location = new System.Drawing.Point(85, 12);
            this.cboNetwork2.Name = "cboNetwork2";
            this.cboNetwork2.Size = new System.Drawing.Size(132, 21);
            this.cboNetwork2.TabIndex = 4;
            // 
            // lblComName2
            // 
            this.lblComName2.AutoSize = true;
            this.lblComName2.Location = new System.Drawing.Point(10, 50);
            this.lblComName2.Name = "lblComName2";
            this.lblComName2.Size = new System.Drawing.Size(45, 13);
            this.lblComName2.TabIndex = 2;
            this.lblComName2.Text = "Com XX";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Mạng";
            // 
            // gbxModem3
            // 
            this.gbxModem3.Controls.Add(this.chkDefault3);
            this.gbxModem3.Controls.Add(this.cboNetwork3);
            this.gbxModem3.Controls.Add(this.lblComName3);
            this.gbxModem3.Controls.Add(this.label5);
            this.gbxModem3.Location = new System.Drawing.Point(13, 268);
            this.gbxModem3.Name = "gbxModem3";
            this.gbxModem3.Size = new System.Drawing.Size(247, 87);
            this.gbxModem3.TabIndex = 4;
            this.gbxModem3.TabStop = false;
            this.gbxModem3.Text = "Modem 3";
            // 
            // chkDefault3
            // 
            this.chkDefault3.AutoSize = true;
            this.chkDefault3.Location = new System.Drawing.Point(146, 46);
            this.chkDefault3.Name = "chkDefault3";
            this.chkDefault3.Size = new System.Drawing.Size(71, 17);
            this.chkDefault3.TabIndex = 6;
            this.chkDefault3.Text = "Mặc định";
            this.chkDefault3.UseVisualStyleBackColor = true;
            this.chkDefault3.CheckedChanged += new System.EventHandler(this.chkDefault_CheckedChanged);
            // 
            // cboNetwork3
            // 
            this.cboNetwork3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNetwork3.FormattingEnabled = true;
            this.cboNetwork3.Location = new System.Drawing.Point(85, 12);
            this.cboNetwork3.Name = "cboNetwork3";
            this.cboNetwork3.Size = new System.Drawing.Size(132, 21);
            this.cboNetwork3.TabIndex = 5;
            // 
            // lblComName3
            // 
            this.lblComName3.AutoSize = true;
            this.lblComName3.Location = new System.Drawing.Point(10, 50);
            this.lblComName3.Name = "lblComName3";
            this.lblComName3.Size = new System.Drawing.Size(45, 13);
            this.lblComName3.TabIndex = 2;
            this.lblComName3.Text = "Com XX";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Mạng";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(185, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Lưu";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FrmDetectModems
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 395);
            this.Controls.Add(this.gbxModem3);
            this.Controls.Add(this.gbxModem2);
            this.Controls.Add(this.gbxModem1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnStart);
            this.Name = "FrmDetectModems";
            this.Text = "Modem Info";
            this.Load += new System.EventHandler(this.FrmDetectModems_Load);
            this.gbxModem1.ResumeLayout(false);
            this.gbxModem1.PerformLayout();
            this.gbxModem2.ResumeLayout(false);
            this.gbxModem2.PerformLayout();
            this.gbxModem3.ResumeLayout(false);
            this.gbxModem3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.GroupBox gbxModem1;
        private System.Windows.Forms.Label lblComName1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbxModem2;
        private System.Windows.Forms.Label lblComName2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gbxModem3;
        private System.Windows.Forms.Label lblComName3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cboNetwork1;
        private System.Windows.Forms.ComboBox cboNetwork2;
        private System.Windows.Forms.ComboBox cboNetwork3;
        private System.Windows.Forms.CheckBox chkDefault1;
        private System.Windows.Forms.CheckBox chkDefault2;
        private System.Windows.Forms.CheckBox chkDefault3;
    }
}