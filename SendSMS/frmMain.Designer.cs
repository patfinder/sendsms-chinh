﻿namespace SendSMS
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.tabSendSMS = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnSend = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.gridNumber = new System.Windows.Forms.DataGridView();
            this.colNumberPhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colContent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnFindModem = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtNumberPhone = new SendSMS.MyTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtContent = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabHistory = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gridHistory = new System.Windows.Forms.DataGridView();
            this.colHistoryNumberPhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colHistoryContent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colHistorySendTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtContentSearch = new System.Windows.Forms.RichTextBox();
            this.txtNumberPhoneSearch = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.bgwSend = new System.ComponentModel.BackgroundWorker();
            this.bgwReceive = new System.ComponentModel.BackgroundWorker();
            this.tmrReceive = new System.Windows.Forms.Timer(this.components);
            this.tabSendSMS.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridNumber)).BeginInit();
            this.panel3.SuspendLayout();
            this.tabHistory.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridHistory)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabSendSMS
            // 
            this.tabSendSMS.Controls.Add(this.tabPage1);
            this.tabSendSMS.Controls.Add(this.tabHistory);
            this.tabSendSMS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabSendSMS.Location = new System.Drawing.Point(0, 0);
            this.tabSendSMS.Name = "tabSendSMS";
            this.tabSendSMS.SelectedIndex = 0;
            this.tabSendSMS.Size = new System.Drawing.Size(755, 549);
            this.tabSendSMS.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tabSendSMS.TabIndex = 0;
            this.tabSendSMS.SelectedIndexChanged += new System.EventHandler(this.tabSendSMS_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel5);
            this.tabPage1.Controls.Add(this.panel4);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(747, 523);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Danh sách gửi";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.progressBar1);
            this.panel5.Controls.Add(this.btnSend);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(3, 452);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(741, 68);
            this.panel5.TabIndex = 6;
            // 
            // progressBar1
            // 
            this.progressBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressBar1.Location = new System.Drawing.Point(0, 45);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(741, 23);
            this.progressBar1.TabIndex = 4;
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(87, 11);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 23);
            this.btnSend.TabIndex = 3;
            this.btnSend.Text = "Gửi";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.gridNumber);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 161);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(741, 359);
            this.panel4.TabIndex = 5;
            // 
            // gridNumber
            // 
            this.gridNumber.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridNumber.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colNumberPhone,
            this.colContent});
            this.gridNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridNumber.Location = new System.Drawing.Point(87, 0);
            this.gridNumber.Name = "gridNumber";
            this.gridNumber.Size = new System.Drawing.Size(654, 359);
            this.gridNumber.TabIndex = 0;
            this.gridNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridNumber_KeyDown);
            // 
            // colNumberPhone
            // 
            this.colNumberPhone.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colNumberPhone.DataPropertyName = "NumberPhone";
            this.colNumberPhone.HeaderText = "Số điện thoại";
            this.colNumberPhone.Name = "colNumberPhone";
            // 
            // colContent
            // 
            this.colContent.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colContent.DataPropertyName = "Content";
            this.colContent.HeaderText = "Nội dung";
            this.colContent.Name = "colContent";
            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(87, 359);
            this.panel6.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnFindModem);
            this.panel3.Controls.Add(this.btnAdd);
            this.panel3.Controls.Add(this.txtNumberPhone);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.txtContent);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(741, 158);
            this.panel3.TabIndex = 4;
            // 
            // btnFindModem
            // 
            this.btnFindModem.Location = new System.Drawing.Point(547, 9);
            this.btnFindModem.Name = "btnFindModem";
            this.btnFindModem.Size = new System.Drawing.Size(121, 23);
            this.btnFindModem.TabIndex = 5;
            this.btnFindModem.Text = "Cấu hình 3G";
            this.btnFindModem.UseVisualStyleBackColor = true;
            this.btnFindModem.Click += new System.EventHandler(this.btnFindModem_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(378, 10);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtNumberPhone
            // 
            this.txtNumberPhone.Location = new System.Drawing.Point(85, 12);
            this.txtNumberPhone.Name = "txtNumberPhone";
            this.txtNumberPhone.Size = new System.Drawing.Size(287, 20);
            this.txtNumberPhone.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Số điện thoại";
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(85, 40);
            this.txtContent.Name = "txtContent";
            this.txtContent.Size = new System.Drawing.Size(368, 107);
            this.txtContent.TabIndex = 2;
            this.txtContent.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nội dung";
            // 
            // tabHistory
            // 
            this.tabHistory.Controls.Add(this.panel2);
            this.tabHistory.Controls.Add(this.panel1);
            this.tabHistory.Location = new System.Drawing.Point(4, 22);
            this.tabHistory.Name = "tabHistory";
            this.tabHistory.Padding = new System.Windows.Forms.Padding(3);
            this.tabHistory.Size = new System.Drawing.Size(747, 523);
            this.tabHistory.TabIndex = 1;
            this.tabHistory.Text = "Lịch sử";
            this.tabHistory.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gridHistory);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 71);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(741, 449);
            this.panel2.TabIndex = 2;
            // 
            // gridHistory
            // 
            this.gridHistory.AllowUserToAddRows = false;
            this.gridHistory.AllowUserToDeleteRows = false;
            this.gridHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridHistory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colHistoryNumberPhone,
            this.colHistoryContent,
            this.colHistorySendTime});
            this.gridHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridHistory.Location = new System.Drawing.Point(0, 0);
            this.gridHistory.Name = "gridHistory";
            this.gridHistory.ReadOnly = true;
            this.gridHistory.Size = new System.Drawing.Size(741, 449);
            this.gridHistory.TabIndex = 0;
            // 
            // colHistoryNumberPhone
            // 
            this.colHistoryNumberPhone.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.colHistoryNumberPhone.DataPropertyName = "NumberPhone";
            this.colHistoryNumberPhone.HeaderText = "Số điện thoại";
            this.colHistoryNumberPhone.Name = "colHistoryNumberPhone";
            this.colHistoryNumberPhone.ReadOnly = true;
            this.colHistoryNumberPhone.Width = 95;
            // 
            // colHistoryContent
            // 
            this.colHistoryContent.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colHistoryContent.DataPropertyName = "Content";
            this.colHistoryContent.HeaderText = "Nội dung";
            this.colHistoryContent.Name = "colHistoryContent";
            this.colHistoryContent.ReadOnly = true;
            // 
            // colHistorySendTime
            // 
            this.colHistorySendTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.colHistorySendTime.DataPropertyName = "SendTime";
            this.colHistorySendTime.HeaderText = "Thời gian";
            this.colHistorySendTime.Name = "colHistorySendTime";
            this.colHistorySendTime.ReadOnly = true;
            this.colHistorySendTime.Width = 76;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtContentSearch);
            this.panel1.Controls.Add(this.txtNumberPhoneSearch);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(741, 68);
            this.panel1.TabIndex = 1;
            // 
            // txtContentSearch
            // 
            this.txtContentSearch.Location = new System.Drawing.Point(265, 7);
            this.txtContentSearch.Name = "txtContentSearch";
            this.txtContentSearch.Size = new System.Drawing.Size(188, 50);
            this.txtContentSearch.TabIndex = 3;
            this.txtContentSearch.Text = "";
            // 
            // txtNumberPhoneSearch
            // 
            this.txtNumberPhoneSearch.Location = new System.Drawing.Point(81, 7);
            this.txtNumberPhoneSearch.Name = "txtNumberPhoneSearch";
            this.txtNumberPhoneSearch.Size = new System.Drawing.Size(122, 20);
            this.txtNumberPhoneSearch.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(81, 34);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Tìm";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(209, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Nội dung";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Số điện thoại";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // bgwSend
            // 
            this.bgwSend.WorkerReportsProgress = true;
            this.bgwSend.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwSend_DoWork);
            this.bgwSend.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwSend_ProgressChanged);
            // 
            // bgwReceive
            // 
            this.bgwReceive.WorkerReportsProgress = true;
            this.bgwReceive.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgCheckIncomeMsg_DoWork);
            this.bgwReceive.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgCheckIncomeMsg_ProgressChanged);
            this.bgwReceive.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwReceive_RunWorkerCompleted);
            // 
            // tmrReceive
            // 
            this.tmrReceive.Tick += new System.EventHandler(this.tmrCheckIncomeMsg_Tick);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(755, 549);
            this.Controls.Add(this.tabSendSMS);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.Text = "Send SMS";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.tabSendSMS.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridNumber)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabHistory.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridHistory)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabSendSMS;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabHistory;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.RichTextBox txtContent;
        private MyTextBox txtNumberPhone;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView gridHistory;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RichTextBox txtContentSearch;
        private System.Windows.Forms.TextBox txtNumberPhoneSearch;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView gridNumber;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.DataGridViewTextBoxColumn colHistoryNumberPhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn colHistoryContent;
        private System.Windows.Forms.DataGridViewTextBoxColumn colHistorySendTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNumberPhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn colContent;
        private System.ComponentModel.BackgroundWorker bgwSend;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnFindModem;
        private System.ComponentModel.BackgroundWorker bgwReceive;
        private System.Windows.Forms.Timer tmrReceive;
    }
}