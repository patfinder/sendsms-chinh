﻿using Nexle.Common;
using SendSMS.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Threading;
using System.Windows.Forms;

namespace SendSMS
{
    public partial class FrmMain : Form
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly DbService dbLib = new DbService();
        private SendSMSLib smsLib = new SendSMSLib();
        private Mutex DeviceMutex = new Mutex();

        public FrmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            var db = new DbService();
//            db.LoadData();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            // TODO: Santify check for device locking
            //if(DeviceMutex.WaitOne(,))

            bgwSend.RunWorkerAsync();
        }

        private void tabSendSMS_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabSendSMS.SelectedTab == tabSendSMS.TabPages["tabHistory"])
            {
                LoadHistory();
            }
        }

        private void LoadHistory()
        {
            string errorMessage = string.Empty;
            List<SendMessageHistory> lstHistory = dbLib.GetHistory(string.Empty, string.Empty, out errorMessage);
            gridHistory.DataSource = lstHistory;
            gridHistory.Show();
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string errorMessage = string.Empty;
            List<SendMessageHistory> lstHistory = dbLib.GetHistory(txtNumberPhoneSearch.Text, txtContentSearch.Text,
                out errorMessage);
            gridHistory.DataSource = lstHistory;
            gridHistory.Show();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string[] lstNumberPhone = txtNumberPhone.Text.Split(new[] {',', ';'});
            var lstNumberPhoneModel = new List<NumberPhoneModel>();
            foreach (DataGridViewRow row in gridNumber.Rows)
            {
                var i = row.DataBoundItem as NumberPhoneModel;
                if (i != null)
                {
                    lstNumberPhoneModel.Add(i);
                }
            }

            foreach (string s in lstNumberPhone)
            {
                lstNumberPhoneModel.Add(new NumberPhoneModel {NumberPhone = s, Content = txtContent.Text});
            }

            var src = new BindingSource();
            src.DataSource = lstNumberPhoneModel;
            gridNumber.DataSource = src;
            gridNumber.Show();
        }

        private void gridNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                DataObject d = gridNumber.GetClipboardContent();
                Clipboard.SetDataObject(d);
                e.Handled = true;
            }
            else if (e.Control && e.KeyCode == Keys.V)
            {
//                PasteInData(ref gridNumber);
                PasteData2(ref gridNumber);
                gridNumber.AllowUserToAddRows = true;
                gridNumber.Update();
                gridNumber.Refresh();
                
            }
        }

        // PasteInData pastes clipboard data into the grid passed to it.
        private void PasteData2(ref DataGridView dgv)
        {
            string[] lstNumberPhone = txtNumberPhone.Text.Split(new[] { ',', ';' });
            var lstNumberPhoneModel = GetListNumberFromGridNumber();
            

            //
            int r = dgv.SelectedCells[0].RowIndex;


            char[] rowSplitter = { '\n', '\r' }; // Cr and Lf.
            char columnSplitter = '\t'; // Tab.
            IDataObject dataInClipboard = Clipboard.GetDataObject();

            string stringInClipboard =
                dataInClipboard.GetData(DataFormats.Text).ToString();

            string[] rowsInClipboard = stringInClipboard.Split(rowSplitter,
                StringSplitOptions.RemoveEmptyEntries);

            int iRow = rowsInClipboard.Length - 1;
            while (iRow >= 0)
            {
                // Split up rows to get individual cells:

                string[] valuesInRow =
                    rowsInClipboard[iRow].Split(columnSplitter);

                var newNumberPhoneModel = new NumberPhoneModel()
                {
                    NumberPhone = valuesInRow[0],
                    Content = valuesInRow[1]
                };
                lstNumberPhoneModel.Insert(r,newNumberPhoneModel);
                
                /*int jCol = 0;
                while (jCol < valuesInRow.Length)
                {
                    if ((dgv.ColumnCount - 1) >= (c + jCol))
                        dgv.Rows[r + iRow].Cells[c + jCol].Value =
                            valuesInRow[jCol];

                    jCol += 1;
                } // end while*/

                iRow -= 1;
            } // end while
            var src = new BindingSource();
            src.DataSource = lstNumberPhoneModel;
            gridNumber.DataSource = src;
        }

        private List<NumberPhoneModel> GetListNumberFromGridNumber()
        {
            var lstNumberPhoneModel = new List<NumberPhoneModel>();
            foreach (DataGridViewRow row in gridNumber.Rows)
            {
                var i = row.DataBoundItem as NumberPhoneModel;
                if (i != null)
                {
                    lstNumberPhoneModel.Add(i);
                }
            }
            return lstNumberPhoneModel;
        }

        private void bgwSend_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
            progressBar1.Text = e.ProgressPercentage.ToString();
        }

        private void bgwSend_DoWork(object sender, DoWorkEventArgs e)
        {
            /*string strContent = txtContent.Text;
            if (string.IsNullOrEmpty(strContent))
                MessageBox.Show("Chưa nhập tin nhắn");*/

            var lstNumberPhoneModel = GetListNumberFromGridNumber();

            var portName = string.Empty;

            // TODO: Change here to use configured modem to send (or suitable modem for current number)
            foreach (string strPort in SerialPort.GetPortNames())
            {
                if (SendSMSLib.IsModem(strPort))
                {
                    portName = strPort;
                }
            }

            // Wait DB Lock for 5 seconds
            if (!DeviceMutex.WaitOne(5000))
            {
                log.Info(string.Format("{0}: Cannot retrieve DB Log. Sending process is cancelled.", LangUtils.GetCurrentMethod()));
                return;
            }

            var no = 0;
            foreach (var numberphoneModel in lstNumberPhoneModel)
            {
                if (smsLib.SendSMS(numberphoneModel.NumberPhone, numberphoneModel.Content, portName))
                //                if (true)
                {
                    dbLib.SaveOutMessage(numberphoneModel.NumberPhone, numberphoneModel.Content);
                }
                no++;
                int percent = (int) (((double)no/lstNumberPhoneModel.Count)*100);
                bgwSend.ReportProgress(percent);
            }

            // Release DB lock
            DeviceMutex.ReleaseMutex();
        }

        private void btnFindModem_Click(object sender, EventArgs e)
        {
            FrmDetectModems form = new FrmDetectModems();
            form.ShowDialog();

            LoadModemList();
        }

        private void LoadModemList()
        {
            //List<KeyValuePair<string, string>> portToNetworks = new List<KeyValuePair<string,string>>();
            //for(int i=1; i<=3; i++)
            //{
            //    string port = (string)Settings.Default["Modem" + i];
            //    string net = (string)Settings.Default["Network" + i];

            //    if(!string.IsNullOrEmpty(port) && !string.IsNullOrEmpty(net))
            //        portToNetworks.Add(new KeyValuePair<string,string>(port, port + " - " + net));
            //}

            //cboModem.DataSource = portToNetworks;
            //cboModem.DisplayMember = "Value";
            //cboModem.ValueMember = "Key";
        }

        private void tmrCheckIncomeMsg_Tick(object sender, EventArgs e)
        {
            // Wait for 5 seconds
            if(DeviceMutex.WaitOne(5000))
                bgwReceive.RunWorkerAsync();
        }

        private void bgwReceive_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DeviceMutex.ReleaseMutex();
        }

        private void backgCheckIncomeMsg_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void backgCheckIncomeMsg_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }
    }
}