﻿using Nexle.Common;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;
using System.Threading;

namespace SendSMS
{
    class SendSMSLib
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //private string PortName = "COM5";
        //private int BaudRate = 115200;
        private SerialPort _serialPort;

        public bool SendSMS(string number, string message, string port)
        {
            try
            {
                // ------------------------------
                // This function worked
                // ------------------------------

                //            string number = "0909551107";
    //            string number = "0933227955";
    //            string message = "Test SMS Prog";
                // Port with name: "PC UI Interface"
//                string port = "COM5";
                _serialPort = new SerialPort(port, 115200); //Replace "COM7" with corresponding port name
                Thread.Sleep(1000);
                _serialPort.Open();
                Thread.Sleep(1000);
                _serialPort.Write("AT+CMGF=1\r");
                Thread.Sleep(1000);
                _serialPort.Write("AT+CMGS=\"" + number + "\"\r\n");
                Thread.Sleep(1000);
                _serialPort.Write(message + "\x1A");
                Thread.Sleep(1000);

                //labelStatus.Text = "Status: Message sent";
                _serialPort.Close();
            }
            catch (Exception ex)
            {
                log.Error(LangUtils.GetCurrentMethod(), ex);
                return false;
            }
            return true;
        }

        public bool ReceiveSMS(string message, string port)
        {
            errorMessage = string.Empty;
            try
            {
                // ------------------------------
                // This function worked
                // ------------------------------

                //            string number = "0909551107";
                //            string number = "0933227955";
                //            string message = "Test SMS Prog";
                // Port with name: "PC UI Interface"
                //                string port = "COM5";
                _serialPort = new SerialPort(port, 115200); //Replace "COM7" with corresponding port name
                Thread.Sleep(1000);
                _serialPort.Open();
                Thread.Sleep(1000);
                _serialPort.Write("AT+CMGF=1\r");
                Thread.Sleep(1000);
                _serialPort.Write("AT+CMGS=\"" + number + "\"\r\n");
                Thread.Sleep(1000);
                _serialPort.Write(message + "\x1A");
                Thread.Sleep(1000);

                //labelStatus.Text = "Status: Message sent";
                _serialPort.Close();
            }
            catch (Exception ex)
            {
                log.Error(LangUtils.GetCurrentMethod(), ex);
                return false;
            }
            return true;
        }

        public bool IsOpen
        {
            get
            {
                return this._serialPort.IsOpen;
            }
        }

        public static bool IsModem(string PortName)
        {

            SerialPort port = null;
            try
            {
                port = new SerialPort(PortName, 9600); //9600 baud is supported by most modems
                port.ReadTimeout = 200;
                port.Open();
                port.Write("AT\r\n");

                //some modems will return "OK\r\n"
                //some modems will return "\r\nOK\r\n"
                //some will echo the AT first
                //so
                for (int i = 0; i < 4; i++) //read a maximum of 4 lines in case some other device is posting garbage
                {
                    string line = port.ReadLine();
                    if (line.IndexOf("OK") >= -1)
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                // You should implement more specific catch blocks so that you would know 
                // what the problem was, ie port may be in use when you are trying
                // to test it so you should let the user know that as he can correct that fault

                return false;
            }
            finally
            {
                if (port != null)
                {
                    port.Close();
                }
            }
        }

    }
}
