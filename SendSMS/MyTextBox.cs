﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace SendSMS
{
    class MyTextBox : TextBox
    {
        protected override void WndProc(ref Message m)
        {
            // Trap WM_PASTE:
            if (m.Msg == 0x302 && Clipboard.ContainsText())
            {
//                this.SelectedText = Clipboard.GetText().Replace('\n', ' ');
                string s = Clipboard.GetText();
                s = Regex.Replace(s, @"[^\d\n]", String.Empty);
                string[] lines = s.Split('\n');
                this.SelectedText = string.Join(";", lines);
                return;
            }
            base.WndProc(ref m);
        }
    }
}
